const SimpleDOM = require('simple-dom');
const createDOMRenderer = require('./parse-config');
const {
  filterPosts,
  getIdFromFilename,
  createCollections,
  isValidTemplate,
} = require('./utils');

function plugin(conciergeConfig, layoutExtension = 'html') {
  const rendererPromise = createDOMRenderer(conciergeConfig);


  //------------------------------------------------------
  // Helper functions, that rely on the plugin scope
  
  const parsePost = async ([filepath, postData]) => {
    const content = JSON.parse(postData.contents.toString());
    const modulePromises = content.modules.map(parseModule);
    const modules = await Promise.all(modulePromises);
    const html = modules.join('');

    const templateName = `${content.metadata.template}.${layoutExtension}`;
    const validTemplate = isValidTemplate(templateName);

    return {
      ...postData,
      layout: validTemplate ? templateName : `default.${layoutExtension}`,
      conciergeMeta: { ...content.metadata, id: getIdFromFilename(filepath) },
      contents: new Buffer(html),
    };
  };
  
  const parseModule = async (moduleData) => {
    const renderer = await rendererPromise;
    const rendered = renderer.render(moduleData);
    const serializer = new SimpleDOM.HTMLSerializer(SimpleDOM.voidMap);
    return serializer.serializeChildren(rendered.result);
  };
  


  //------------------------------------------------------
  // The actual plugin function

  return async (files, metalsmith, done) => {
    // Filter out all files, that are not posts
    const filteredPosts = filterPosts(files);
    
    // Parse concierge post data
    const postData = await Promise.all(Object.entries(filteredPosts).map(parsePost));

    // Assign parsed data to the corresponding post
    const parsedPosts = Object.keys(filteredPosts)
      .reduce((posts, filename, index) => ({ ...posts, [filename]: postData[index]}), {});

    // Override metalsmith files with the parsed data
    Object.entries(parsedPosts).forEach(([filename, data]) => {
      delete files[filename];
      files[`${data.conciergeMeta.filename}.html`] = data;
    });
    
    // Add collections to the global metadata
    const collections = createCollections(files);
    const metadata = metalsmith.metadata();
    metadata.conciergeCollections = collections;

    setImmediate(done);
  };

}

module.exports = plugin;