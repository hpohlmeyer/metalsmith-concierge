const MobiledocDOMRenderer = require('mobiledoc-dom-renderer').default;
const SimpleDOM = require('simple-dom');
const rollup = require('rollup');


async function parseConfig(configPath) {
  // Rollup the config
  const bundle = await rollup.rollup({ input: configPath });
  const { code } = await bundle.generate({
    format: 'iife',
    name: 'conciergeProject',
  });

  // Convert it to a js object.
  const config = eval(`(function () {${code} return conciergeProject; })()`);

  // Merge editor config with defaults
  const options = {
    atoms: config.editor.atoms || [],
    cards: config.editor.cards || [],
  };

  return options;
}

async function createDOMRenderer(configPath) {
  const options = await parseConfig(configPath);
  const renderer = new MobiledocDOMRenderer({
    ...options,
    dom: new SimpleDOM.Document(),
  });

  return renderer;
} 

module.exports = createDOMRenderer;