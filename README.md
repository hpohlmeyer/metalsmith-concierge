# metalsmith-concierge

> Work with concierge in metalsmith.

## Usage
When metalsmith is set up as a generator in the concierge config, the `CONCIERGE_CONTENT_DIR`, `CONCIERGE_BUILD_DIR` and `CONCIERGE_CONFIG_PATH` are set. Otherwise you have to set the paths manually, but this is not recommended.

```js
const metalsmith = require('metalsmith');
const concierge = require('concierge');

metalsmith(__dirname)
  .source(process.env.CONCIERGE_CONTENT_DIR)
  .destination(process.env.CONCIERGE_BUILD_DIR)
  .clean(true)
  .use(concierge(process.env.CONCIERGE_CONFIG_PATH))
  // The file contents have been compiled to html at this point.
  // Each file also contains a `conciergeMeta` property, with all
  // the meta data from concierge.
  .build(error => if (error) { throw error; });
```

## Collections
This plugin includes support for collections in order to create navigations or other kinds of collections. It writes the `conciergeCollections` object to the global metalsmith metadata. Collections are organized by tags. Every tag has its own collection. Let’s say we want to create a navigation for our site and use handlebars as our templating engine.

The first task you have to do is to give every post you want to see in the navigation the `navigation` tag (you can name it as you like). After that you can include it in your template. For handlebars it looks like this:

```html
{{#if conciergeCollections.navigation}}
  <nav>
    <ul>
      {{#each conciergeCollections.navigation }}
        <li><a href="{{this.path}}">{{this.name}}</a></li>
      {{/each}}
    </ul>
  </nav>
{{/if}}
```

## Layouts
You can use a templating engine to include your concierge data in html templates. The name of the template has to conform to the name of the template, that is used by default. This means you need at least one `default` template. The second argument of the concierge plugin sets the file extension of your templates. If you use [_metalsmith-layouts_](https://github.com/ismay/metalsmith-layouts) with _handlebars_ it looks like this:

```js
const metalsmith = require('metalsmith');
const concierge = require('concierge');
const layouts = require('metalsmith-layouts');

metalsmith(__dirname)
  .source(process.env.CONCIERGE_CONTENT_DIR)
  .destination(process.env.CONCIERGE_BUILD_DIR)
  .clean(true)
  .use(concierge(process.env.CONCIERGE_CONFIG_PATH, 'hbs'))
  .use(layouts({
    default: 'default.hbs',
    pattern: '**/*.html'
  }))
  .build(error => if (error) { throw error; });
```
 