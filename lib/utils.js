const fs = require('fs');
const { promisify } = require('util');
const access = promisify(fs.access);

// Helper functions
const isPost = (filename) => /post(\d|[a-z])+\.json$/.test(filename);

const filterPosts = (files) => Object.entries(files)
  .reduce((posts, [filename, data]) => isPost(filename) ? { ...posts, [filename]: data} : posts, {});

const getIdFromFilename = (filename) => {
  const id = /post((\d|[a-z])+)\.json$/.exec(filename)[1];
  if (!id) { throw new Error(`Could not get the id from filename "${filename}"`)}
  return id;
}

const createCollections = (files) => {
  return Object.entries(files).reduce((collections, [filename, data]) => {
    if (!data.conciergeMeta) { return collections; }
    const tempCollections = { ...collections };
    data.conciergeMeta.tags
      .forEach(tag => {
        const entry = { name: data.conciergeMeta.title, path: filename };
        return tempCollections[tag] = tempCollections[tag] ? [...tempCollections[tag], entry] : [entry];
      });
    return tempCollections;
  }, {});
}

const isValidTemplate = async (templatePath) => {
  try {
    await access(templatePath)
    return true;
  } catch(err) {
    return false;
  }
}

module.exports = {
  isPost,
  filterPosts,
  getIdFromFilename,
  createCollections,
  isValidTemplate,
};